var evoble = {

	// Device list.
	devices:{},

	beginScan:function(){

		app.updateText('beginScan');
		evoble.startScan(evoble.deviceFound);
		app.updateText('beginScan..scanning ');
	},
	startScan :function(callbackFun)
	{
		evoble.stopScan();

		evothings.ble.startScan(
			function(device)
			{
				// Report success.
				callbackFun(device, null);
			},
			function(errorCode)
			{
				// Report error.
				callbackFun(null, errorCode);
			}
		);
	},
	stopScan:function()
	{
		evothings.ble.reset();
	},
	deviceFound:function(device, errorCode)
	{
		if (device)
		{
			// Insert the device into table of found devices.
			evoble.devices[device.address] = device;

			// Display device in UI.
			evoble.displayDeviceList();

			/* Stop the scan
			*/

			evoble.stopScan();
			//evoble.devices = {};
			evoble.connect(device.address,device.name);
			//evoble.displayDeviceList();
		}
		else if (errorCode)
		{
			app.updateText('Scan Error: ' + errorCode);
		}
	},
	displayDeviceList :function()
	{
		// Clear device list.
		
		var i = 1;
		app.updateText(JSON.stringify(evoble.devices));
		/*$.each(evoble.devices, function(key, device)
		{
			// Compute a display percent width value from signal strength.
			// rssi is a negative value, zero is max signal strength.
			var rssiWidth = Math.max(0, (100 + device.rssi));

			// Set background color for this item.
			var bgcolor = i++ % 2 ? 'rgb(225,225,225)' : 'rgb(245,245,245)';

			// Create a div tag to display sensor data.
			var element = 
				'<div class="device-info" style="background:' + bgcolor + ';">'
				//'<div class="device-info" style="background:rgb(200,200,200);">'
				+	'<b>' + device.name + '</b><br/>'
				+	device.address + '<br/>'
				+	device.rssi + '<br/>'
				+ 	'<div style="background:rgb(255,0,0);height:20px;width:'
				+ 		rssiWidth + '%;"></div>'
				+ '</div>'
			
			
			$('#found-devices').append(element);
			app.updateText(element);
		});*/
	},
	connect:function(address, name){
		evothings.ble.connect(address, function(r)
		{
			//app.deviceHandle = r.deviceHandle;
			app.updateText('connect '+r.deviceHandle+' state '+r.state);
			//console.log('connect '+r.deviceHandle+' state '+r.state);
			//document.getElementById('deviceState').innerHTML = r.state;
			if (r.state == 2) // connected
			{
				app.updateText('connected-----');
				evoble.getServices(r.deviceHandle);  //Get the services
			}
		}, function(errorCode)
		{
			app.updateText('connect error: ' + errorCode);
			//console.log('connect error: ' + errorCode);
		});
	},
	writeCharacteristic:function(device,charhandle){
		
			app.updateText('_____ writeCharacteristic _______ ');
			var data = evothings.ble.toUtf8("test1");
			evothings.ble.writeCharacteristic(
				device,
				charhandle,
				data,
				function(){
						app.updateText('_____ writeCharacteristic Success_______ ');
						try{
							evoble.readCharacteristic(device,charhandle);
						}catch(e){
							app.updateText('errror' +e);
						}
				},
				function(){
						app.updateText('_____ writeCharacteristic Fail_______ ');
				});
			
		
	},
	readCharacteristic:function(device,charhandle){
		app.updateText('_____ readCharacteristic _______ ');
		evothings.ble.readCharacteristic(
			device,
			charhandle,
			function(data){
					app.updateText('_____ readCharacteristic Success_______ DATA : ' +evothings.ble.fromUtf8(data) +' ==||');
			},
			function(err){
					app.updateText('_____ writeCharacteristic Fail_______ ' + err);
			});
	},
	getCharacteristics:function(device,service){
		app.updateText('getCharacteristics : device' + device +' service '+ service.handle);
		evothings.ble.characteristics(device,service.handle, 
					function(chars) 
					{
						app.updateText('success characteristics : === '+JSON.stringify(chars));
						if(chars.length > 3)
						{
							for(c in chars)
							{
								app.updateText('_____ chars[c].handle_______ = :'+chars[c].handle);
								if(chars[c].handle == 19)
								{
									evoble.writeCharacteristic(device,chars[c].handle);
								}
							}
							
						}
					},
					function(err){
						app.updateText('errr characteristics : === '+ err);
					});
	},
	getServices:function(deviceHandle)
	{
		app.updateText('getServices : === ' +deviceHandle);
		try{
			
			evothings.ble.services(deviceHandle, function(services) {
				app.updateText('success service : === ');
				for (var si in services)
				{
					var s = services[si];
					app.updateText('si service : === '+ JSON.stringify(s));
					evoble.getCharacteristics(deviceHandle,s)
				}

				/*
				for (var si in services)
				{
					var s = services[si];
					app.updateText('s'+s.handle+': '+s.type+' '+s.uuid+'. '+s.characteristics.length+' chars.');
					//console.log('s'+s.handle+': '+s.type+' '+s.uuid+'. '+s.characteristics.length+' chars.');

					
					for (var ci in s.characteristics)
					{
						var c = s.characteristics[ci];
						app.updateText(' c'+c.handle+': '+c.uuid+'. '+c.descriptors.length+' desc.');
						//console.log(' c'+c.handle+': '+c.uuid+'. '+c.descriptors.length+' desc.');
						//app.updateText(evoble.formatFlags('  properties', c.properties, evothings.ble.property));
						//console.log(formatFlags('  properties', c.properties, ble.property));
						//app.updateText(evoble.formatFlags('  writeType', c.writeType, evothings.ble.writeType));
						//console.log(formatFlags('  writeType', c.writeType, ble.writeType));

						for (var di in c.descriptors)
						{
							var d = c.descriptors[di];
							app.updateText('  d'+d.handle+': '+d.uuid);
							//console.log('  d'+d.handle+': '+d.uuid);

							//var $lvi = $lv.addListViewItem({text: 'd'+d.handle+': '+d.uuid});

							// This be the human-readable name of the characteristic.
							/*if (d.uuid == "00002901-0000-1000-8000-00805f9b34fb")
							{
								var h = d.handle;
								console.log("rd "+h);
								// need a function here for the closure, so that variables h, ch, dli retain proper values.
								// without it, all strings would be added to the last descriptor.
								function f(h, c, lvi)
								{
									ble.readDescriptor(deviceHandle, h, function(data)
									{
										var s = ble.fromUtf8(data);
										console.log("rdw "+h+": "+s);
										c.collapsibleTitleElm().prepend(s + ' ');
									},
									function(errorCode)
									{
										console.log("rdf "+h+": "+errorCode);
										lvi.prepend('rdf ' + errorCode);
									});
								}
								f(h, $c, $lvi);
							}
						}
					}
				}*/
				
			
			}, function(errorCode)
			{
				app.updateText('readAllServiceData error: ' + errorCode);
				//console.log('readAllServiceData error: ' + errorCode);
			});
		}catch(e){
			app.updateText('characteristics error: ' + e);
		}
	},
	formatFlags:function(name, flags, translation)
	{
		var str = name+':';
		for (var key in translation) {
			if((flags & key) != 0)
				str += ' '+translation[key];
		}
		return str;
	}


}