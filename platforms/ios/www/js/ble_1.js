var ble = {

	addressKey : "address",
    scanTimer:null,
    connectTimer:null,
    reconnectTimer:null,
    iOSPlatform :"iOS",
    androidPlatform:"Android",

    
    batteryServiceUuid:"180f",
    batteryLevelCharacteristicUuid:"2a19",
	
    Scratch1ServiceUuid:"a495ff20-c5b1-4b44-b512-1370f02d74de", 
    Scratch1CharacteristicUuid:"a495ff21-c5b1-4b44-b512-1370f02d74de", 
    // Application Constructor

	initialize:function(){
		 app.updateText("BLE");
		 bluetoothle.initialize(ble.initializeSuccess, ble.initializeError);
	},
	initializeSuccess:function(obj){

          //app.updateText("initialized");
          if (obj.status == "initialized")
          {
            app.updateText("initialized");
            var address = window.localStorage.getItem(ble.addressKey);
            if (address == null)
            {
                app.updateText('start scanning');
                console.log("Bluetooth initialized successfully, starting scan for heart rate devices.");
                var paramsObj = {"serviceUuids":[]};
                bluetoothle.startScan(ble.startScanSuccess, ble.startScanError, paramsObj);
            }
            else
            {
                app.updateText("connecting");
                ble.connectDevice(address);
            }
          }
          else
          {
            app.updateText("Unexpected initialize status: " + obj.status);
            console.log("Unexpected initialize status: " + obj.status);
          }
    },
    initializeError:function(obj)
    {
         app.updateText("Initialize error: " + obj.error + " - " + obj.message);
      console.log("Initialize error: " + obj.error + " - " + obj.message);
    },
    startScanSuccess:function(obj)
    {
      app.updateText("startScanSuccess");
      if (obj.status == "scanResult")
      {
        app.updateText("Stopping scan..");
        console.log("Stopping scan..");
        bluetoothle.stopScan(ble.stopScanSuccess, ble.stopScanError);
        ble.clearScanTimeout();
        app.updateText(obj);
        window.localStorage.setItem(ble.addressKey, obj.address);
        ble.connectDevice(obj.address);
      }
      else if (obj.status == "scanStarted")
      {
        app.updateText("Scan was started successfully, stopping in 10");
        console.log("Scan was started successfully, stopping in 10");
        ble.scanTimer = setTimeout(ble.scanTimeout, 10000);
      }
      else
      {
        app.updateText("Unexpected start scan status: " + obj.status);
        console.log("Unexpected start scan status: " + obj.status);
      }
    },
    startScanError:function(obj)
    {
      app.updateText("Start scan error: " + obj.error + " - " + obj.message);
      console.log("Start scan error: " + obj.error + " - " + obj.message);
    },
    scanTimeout:function()
    {
      app.updateText("Scanning time out, stopping");
      console.log("Scanning time out, stopping");
      bluetoothle.stopScan(ble.stopScanSuccess, ble.stopScanError);
    },
    clearScanTimeout:function()
    { 
        app.updateText("Clearing scanning timeout");
        console.log("Clearing scanning timeout");
      if (ble.scanTimer != null)
      {
        clearTimeout(ble.scanTimer);
      }
    },
     stopScanSuccess:function(obj)
    {
      if (obj.status == "scanStopped")
      {
         app.updateText("Scan was stopped successfully");
        console.log("Scan was stopped successfully");
      }
      else
      {
        app.updateText("Unexpected stop scan status: " + obj.status);
        console.log("Unexpected stop scan status: " + obj.status);
      }
    },
    stopScanError:function(obj)
    {
      app.updateText("Stop scan error: " + obj.error + " - " + obj.message);
      console.log("Stop scan error: " + obj.error + " - " + obj.message);
    },
    connectDevice:function(address)
    {
      
      app.updateText("Begining connection to: " + address + " with 5 second timeout");
      console.log("Begining connection to: " + address + " with 5 second timeout");
      var paramsObj = {"address":address};
      bluetoothle.connect(ble.connectSuccess, ble.connectError, paramsObj);
      ble.connectTimer = setTimeout(ble.connectTimeout, 5000);
    },
    connectSuccess:function(obj)
    {
      if (obj.status == "connected")
      {
         app.updateText("Connected to : " + obj.name + " - " + obj.address);
        console.log("Connected to : " + obj.name + " - " + obj.address);

        ble.clearConnectTimeout();

        console.log("Discovering heart rate service");
        var paramsObj = {"serviceUuids":[ble.Scratch1ServiceUuid]};
        bluetoothle.services(ble.servicesBatterySuccess, ble.servicesBatteryError, paramsObj);
        //this.tempDisconnectDevice();
      }
      else if (obj.status == "connecting")
      {
        app.updateText("Connecting to : " + obj.name + " - " + obj.address);
        console.log("Connecting to : " + obj.name + " - " + obj.address);
      }
        else
      {
        app.updateText("Unexpected connect status: " + obj.status);
        console.log("Unexpected connect status: " + obj.status);
        ble.clearConnectTimeout();
      }
    },
     connectError:function(obj)
    {
         app.updateText("Connect error: " + obj.error + " - " + obj.message);
      console.log("Connect error: " + obj.error + " - " + obj.message);
      ble.clearConnectTimeout();
    },
    connectTimeout:function()
    {
         app.updateText("Connection timed out");
      console.log("Connection timed out");
    },
     clearConnectTimeout:function()
    { 

        app.updateText("Connection timed out");
        console.log("Clearing connect timeout");
      if (ble.connectTimer != null)
      {
        clearTimeout(ble.connectTimer);
      }
    },
    servicesBatterySuccess:function(obj)
    {
      if (obj.status == "discoveredServices")
      {
        var serviceUuids = obj.serviceUuids;
        for (var i = 0; i < serviceUuids.length; i++)
        {
          var serviceUuid = serviceUuids[i];
            app.updateText("services = "+serviceUuid);
          if (serviceUuid == ble.Scratch1ServiceUuid)
          {
             app.updateText("Found Scratch service, now finding characteristic");
            //console.log("Found battery service, now finding characteristic");
            var paramsObj = {"serviceUuid":ble.Scratch1ServiceUuid, "characteristicUuids":[ble.Scratch1CharacteristicUuid]};
            bluetoothle.characteristics(ble.characteristicsBatterySuccess, ble.characteristicsBatteryError, paramsObj);
            return;
          }
        }
         app.updateText("Error: battery service not found");
        console.log("Error: battery service not found");
      }
        else
      {
        app.updateText("Unexpected services battery status: " + obj.status);
        console.log("Unexpected services battery status: " + obj.status);
      }
      ble.disconnectDevice();
    },
    servicesBatteryError:function(obj)
    {
      app.updateText("Services battery error: " + obj.error + " - " + obj.message);
      console.log("Services battery error: " + obj.error + " - " + obj.message);
      ble.disconnectDevice();
    },
     characteristicsBatteryError:function(obj)
    {
        app.updateText("Characteristics battery error: " + obj.error + " - " + obj.message);
      console.log("Characteristics battery error: " + obj.error + " - " + obj.message);
      ble.disconnectDevice();
    },
    characteristicsBatterySuccess:function(obj)
    {
         app.updateText("characteristicsBatterySuccess");
      if (obj.status == "discoveredCharacteristics")
      {
        var characteristicUuids = obj.characteristicUuids;
        for (var i = 0; i < characteristicUuids.length; i++)
        {
          var characteristicUuid = characteristicUuids[i];
          //app.updateText("characteristicUuid : " + i + " - " + characteristicUuid );
          if (characteristicUuid == ble.Scratch1CharacteristicUuid)
          {
            ble.readBatteryLevel();
            return;
          }
        }
         app.updateText("Error: Battery characteristic not found.");
        console.log("Error: Battery characteristic not found.");
      }
        else
      {
        app.updateText("Unexpected characteristics battery status: " + obj.status);
        console.log("Unexpected characteristics battery status: " + obj.status);
      }
      ble.disconnectDevice();
    },
     readBatteryLevel:function()
    {
      console.log("Reading battery level");
      var paramsObj = {"serviceUuid":ble.Scratch1ServiceUuid, "characteristicUuid":ble.Scratch1CharacteristicUuid};
      bluetoothle.read(ble.readSuccess, ble.readError, paramsObj);
    },
    readSuccess:function(obj)
    {
        if (obj.status == "read")
        {
            //var bytes = bluetoothle.encodedStringToBytes(obj.value);
			app.updateText("Data : " + JSON.stringify(obj));
			
			var res = String.fromCharCode.apply(null, new Uint8Array(obj.value));
			app.updateText("Data 1 : " + res);
            //console.log("Battery level: " + bytes[0]);
           
            /*setInterval(function(){
                app.updateText("running getRSSI");
                ble.getRSSI();}, 3000);*/
		     var bytes = bluetoothle.stringToBytes("on");
		     var paramsObj = {"value":"","serviceUuid":ble.Scratch1ServiceUuid, "characteristicUuid":ble.Scratch1CharacteristicUuid};
		     bluetoothle.write(ble.writeSucccess, ble.writeError, paramsObj);
            
        }
        else
      {
        console.log("Unexpected read status: " + obj.status);
        ble.disconnectDevice();
      }
    },
    readError:function(obj)
    {
      console.log("Read error: " + obj.error + " - " + obj.message);
      ble.disconnectDevice();
    },
	writeSucccess:function(obj){
		app.updateText("writeSucccess  " +JSON.stringify(obj) );
	},
	writeError:function(obj){
		app.updateText("writeError  " + JSON.stringify(obj) );
	},


     disconnectDevice:function()
    {
      bluetoothle.disconnect(ble.disconnectSuccess, ble.disconnectError);
    },
    disconnectSuccess:function(obj)
    {
        if (obj.status == "disconnected")
        {
            console.log("Disconnect device");
            ble.closeDevice();
        }
        else if (obj.status == "disconnecting")
        {
            console.log("Disconnecting device");
        }
        else
      {
        console.log("Unexpected disconnect status: " + obj.status);
      }
    },
    disconnectError:function(obj)
    {
      console.log("Disconnect error: " + obj.error + " - " + obj.message);
    },
    closeDevice:function()
    {
      bluetoothle.close(ble.closeSuccess, ble.closeError);
    },
    closeSuccess:function(obj)
    {
        if (obj.status == "closed")
        {
            console.log("Closed device");
        }
        else
      {
        console.log("Unexpected close status: " + obj.status);
      }
    },
    closeError:function(obj)
    {
      console.log("Close error: " + obj.error + " - " + obj.message);
    },
    getRSSI:function(){
        bluetoothle.rssi(ble.rssiSuccessCallback, ble.rssiErrorCallback);
    },
    rssiSuccessCallback:function(obj){
         app.updateText("RSSI level: " + obj.rssi);
    },
    rssiErrorCallback:function(obj){
        app.updateText("Error RSSI: " + obj);
    }

};