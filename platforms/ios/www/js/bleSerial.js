var bleSerial = {

	init:function(){
		 app.updateText("initializing BLESerial");
		 
 		 
		
		 bleSerial.getList();
	},
	getList:function(){
		app.updateText("getList BLESerial");
		 //bluetoothSerial.list(bleSerial.ondevicelist, bleSerial.generateFailureFunction);
		 bluetoothSerial.connect("", bleSerial.connectSuccess, bleSerial.connectFailure);
	},
	ondevicelist:function(devices){
		var deviceId;
		app.updateText("ondevicelist BLESerial");
        app.updateText("---|device found = " + JSON.stringify(devices));
        /*devices.forEach(function(device) {
            if (device.hasOwnProperty("uuid")) { // TODO https://github.com/don/BluetoothSerial/issues/5
                deviceId = device.uuid;
            } else if (device.hasOwnProperty("address")) {
                deviceId = device.address;
            } else {
                deviceId = "ERROR " + JSON.stringify(device);
            }
            app.updateText("---|device found = " + JSON.stringify(device));
        });*/
	},
	generateFailureFunction:function(err){
		app.updateText("generateFailureFunction BLESerial " + JSON.stringify(err));
	},
	connectSuccess:function(dd){
		app.updateText("connectSuccess BLESerial " + JSON.stringify(dd));
		bluetoothSerial.subscribe("\n", bleSerial.onmessage1, bleSerial.generateFailureFunction);
		//bleSerial.sendData();
		/*
		bluetoothSerial.isConnected(
		    function() 
			{ 
		        app.updateText("Bluetooth is connected");
				bluetoothSerial.readRSSI(
				    function(rssi) 
					{ 
				        app.updateText(rssi);
						
					    /*bluetoothSerial.unsubscribe(
							function () {
								app.updateText("unsubscribe success ");
					        },function(e){
					        	app.updateText("unsubscribe failed " + e);
					        }
						);
				    }
				);  
		    },
		    function() { 
		        app.updateText("Bluetooth is *not* connected");
		    }
		); */
	},
	connectFailure:function(dd){
		app.updateText("connectFailure BLESerial " + JSON.stringify(dd));
	},
	
	sendData: function() {
		app.updateText("sending data");
		//var text = bleSerial.toUtf8("hello\n");
		var text = "hello \n";
	        bluetoothSerial.write(text, bleSerial.sendSuccess,bleSerial.sendFail);
	},
	sendFail:function(e){
		 app.updateText("sendFail " + JSON.stringify(e));
	},
	sendSuccess:function(e){
		 app.updateText("sendSuccess " + JSON.stringify(e));
	},
	onmessage1: function(message) {
	         app.updateText("connectFailure BLESerial " +message);
	},
	generateFailureFunction: function(message) {
	         app.updateText("generateFailureFunction BLESerial " + JSON.stringify(message));
	},
	readValue:function(){
	    bluetoothSerial.read(
			function(d){
				 app.updateText("read success: " + d);
			},
			function(d){
				 app.updateText("read fail :" + d);
			}
		);
	},
	fromUtf8:function(a) {
		return decodeURIComponent(escape(String.fromCharCode.apply(null, new Uint8Array(a))));
	},

	/** Converts a JavaScript String to an Uint8Array containing UTF-8 data.
	* @param {string} s
	* @returns Uint8Array
	*/
	toUtf8:function(s) {
		var strUtf8 = unescape(encodeURIComponent(s));
		var ab = new Uint8Array(strUtf8.length);
		for (var i = 0; i < strUtf8.length; i++) {
			ab[i] = strUtf8.charCodeAt(i);
		}
		return ab;
	}
	

};