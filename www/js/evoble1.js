var evoble = {

	// Device list.
	devices:{},
	devicehandle:"",
	scratch1handle:"",
    Scratch1ServiceUuid:"a495ff20-c5b1-4b44-b512-1370f02d74de", 
    Scratch1CharacteristicUuid:"a495ff21-c5b1-4b44-b512-1370f02d74de", 
    Scratch2CharacteristicUuid:"a495ff22-c5b1-4b44-b512-1370f02d74de", 
	Scratch1Descriptor1Uuid:"00002901-0000-1000-8000-00805f9b34fb",
	Scratch1Descriptor2Uuid:"00002902-0000-1000-8000-00805f9b34fb",

	beginScan:function(){

		app.updateText('beginScan');
		evoble.startScan(evoble.deviceFound);
		app.updateText('beginScan..scanning ');
	},
	startScan :function(callbackFun)
	{
		evoble.stopScan();

		evothings.ble.startScan(
			function(device)
			{
				// Report success.
				callbackFun(device, null);
			},
			function(errorCode)
			{
				// Report error.
				callbackFun(null, errorCode);
			}
		);
	},
	stopScan:function()
	{
		evothings.ble.reset();
	},
	deviceFound:function(device, errorCode)
	{
		if (device)
		{
			// Insert the device into table of found devices.
			evoble.devices[device.address] = device;
			
			// Display device in UI.
			evoble.displayDeviceList();

			/* Stop the scan
			*/

			evoble.stopScan();
			//evoble.devices = {};
			evoble.connect(device.address,device.name);
			//evoble.displayDeviceList();
		}
		else if (errorCode)
		{
			app.updateText('Scan Error: ' + errorCode);
		}
	},
	displayDeviceList :function()
	{
		// Clear device list.
		
		var i = 1;
		app.updateText(JSON.stringify(evoble.devices));
		/*$.each(evoble.devices, function(key, device)
		{
			// Compute a display percent width value from signal strength.
			// rssi is a negative value, zero is max signal strength.
			var rssiWidth = Math.max(0, (100 + device.rssi));

			// Set background color for this item.
			var bgcolor = i++ % 2 ? 'rgb(225,225,225)' : 'rgb(245,245,245)';

			// Create a div tag to display sensor data.
			var element = 
				'<div class="device-info" style="background:' + bgcolor + ';">'
				//'<div class="device-info" style="background:rgb(200,200,200);">'
				+	'<b>' + device.name + '</b><br/>'
				+	device.address + '<br/>'
				+	device.rssi + '<br/>'
				+ 	'<div style="background:rgb(255,0,0);height:20px;width:'
				+ 		rssiWidth + '%;"></div>'
				+ '</div>'
			
			
			$('#found-devices').append(element);
			app.updateText(element);
		});*/
	},
	connect:function(address, name){
		evothings.ble.connect(address, function(r)
		{
			//app.deviceHandle = r.deviceHandle;
			evoble.devicehandle = r.deviceHandle;
			app.updateText('connect '+r.deviceHandle+' state '+r.state);
			//console.log('connect '+r.deviceHandle+' state '+r.state);
			//document.getElementById('deviceState').innerHTML = r.state;
			if (r.state == 2) // connected
			{
				app.updateText('connected-----');
				evoble.getServices(r.deviceHandle);  //Get the services
			}
		}, function(errorCode)
		{
			app.updateText('connect error: ' + errorCode);
			//console.log('connect error: ' + errorCode);
		});
	},
	getServices:function(deviceHandle)
	{
		app.updateText('getServices : === ' +deviceHandle);
		try{
			
			evothings.ble.services(deviceHandle, function(services) {
				app.updateText('success service : === ');
				for (var si in services)
				{
					var s = services[si];
					if(s.uuid == evoble.Scratch1ServiceUuid){
						app.updateText('si service : === '+ JSON.stringify(s));
						evoble.getCharacteristics(deviceHandle,s)
					}
					
				}

				/*
				for (var si in services)
				{
					var s = services[si];
					app.updateText('s'+s.handle+': '+s.type+' '+s.uuid+'. '+s.characteristics.length+' chars.');
					//console.log('s'+s.handle+': '+s.type+' '+s.uuid+'. '+s.characteristics.length+' chars.');

					
					for (var ci in s.characteristics)
					{
						var c = s.characteristics[ci];
						app.updateText(' c'+c.handle+': '+c.uuid+'. '+c.descriptors.length+' desc.');
						//console.log(' c'+c.handle+': '+c.uuid+'. '+c.descriptors.length+' desc.');
						//app.updateText(evoble.formatFlags('  properties', c.properties, evothings.ble.property));
						//console.log(formatFlags('  properties', c.properties, ble.property));
						//app.updateText(evoble.formatFlags('  writeType', c.writeType, evothings.ble.writeType));
						//console.log(formatFlags('  writeType', c.writeType, ble.writeType));

						for (var di in c.descriptors)
						{
							var d = c.descriptors[di];
							app.updateText('  d'+d.handle+': '+d.uuid);
							//console.log('  d'+d.handle+': '+d.uuid);

							//var $lvi = $lv.addListViewItem({text: 'd'+d.handle+': '+d.uuid});

							// This be the human-readable name of the characteristic.
							/*if (d.uuid == "00002901-0000-1000-8000-00805f9b34fb")
							{
								var h = d.handle;
								console.log("rd "+h);
								// need a function here for the closure, so that variables h, ch, dli retain proper values.
								// without it, all strings would be added to the last descriptor.
								function f(h, c, lvi)
								{
									ble.readDescriptor(deviceHandle, h, function(data)
									{
										var s = ble.fromUtf8(data);
										console.log("rdw "+h+": "+s);
										c.collapsibleTitleElm().prepend(s + ' ');
									},
									function(errorCode)
									{
										console.log("rdf "+h+": "+errorCode);
										lvi.prepend('rdf ' + errorCode);
									});
								}
								f(h, $c, $lvi);
							}
						}
					}
				}*/
				
			
			}, function(errorCode)
			{
				app.updateText('readAllServiceData error: ' + errorCode);
				//console.log('readAllServiceData error: ' + errorCode);
			});
		}catch(e){
			app.updateText('characteristics error: ' + e);
		}
	},
	
		getCharacteristics:function(device,service){
			app.updateText('getCharacteristics : device' + device +' service '+ service.handle);
			evothings.ble.characteristics(device,service.handle, 
						function(chars) 
						{
							app.updateText('success characteristics : === '+JSON.stringify(chars));
							
							for(var c in chars)
							{
								app.updateText('c : === '+ JSON.stringify(chars[c]));
								if(chars[c].uuid == evoble.Scratch1CharacteristicUuid){
									app.updateText('Found  Scratch1CharacteristicUuid');
									//evoble.startReading(devicehandle);
									//var data = evothings.ble.toUtf8("temp");
									//evoble.writeCharacteristic(device,chars[c].handle,data);
									evoble.scratch1handle = chars[c].handle;
									
									evoble.startReading(device);
								}
							}
							/*if(chars.length > 3)
							{
								for(c in chars)
								{
									app.updateText('_____ chars[c].handle_______ = :'+chars[c].handle);
									if(chars[c].handle == 19)
									{
										evoble.writeCharacteristic(device,chars[c].handle);
									}
								}
							
							}*/
						},
						function(err){
							app.updateText('errr characteristics : === '+ err);
						});
		},
		writebtn:function(){
			var data = evothings.ble.toUtf8("temp");
			evoble.writeCharacteristic(evoble.devicehandle,evoble.scratch1handle,data);
			
		},
	writeCharacteristic:function(device,charhandle,data){
		
			app.updateText('_____ writeCharacteristic _______ ');
			
			
			evothings.ble.writeCharacteristic(
				device,
				charhandle,
				data,
				function(){
						app.updateText('_____ writeCharacteristic Success_______ ');
						try{
							evoble.readCharacteristic(device,charhandle);
						}catch(e){
							app.updateText('errror' +e);
						}
				},
				function(){
						app.updateText('_____ writeCharacteristic Fail_______ ');
				});
			
		
	},
	readCharacteristic:function(device,charhandle){
		app.updateText('_____ readCharacteristic _______ ');
		evothings.ble.readCharacteristic(
			device,
			charhandle,
			function(data){
					app.updateText('_____ readCharacteristic Success_______ DATA : ' +evothings.ble.fromUtf8(data) +' ==||');
					evoble.getDescriptors(device,charhandle);
			},
			function(err){
					app.updateText('_____ writeCharacteristic Fail_______ ' + err);
			});
	},
	getDescriptors:function(devicehandle,charhandle){
		
		evothings.ble.descriptors(devicehandle,8,
		function(data){
			app.updateText('success descriptors : === '+JSON.stringify(data));
			evoble.startReading(devicehandle);
			
		},function(data){
			app.updateText('error descriptors : === '+JSON.stringify(data));
		});
		/*exports.descriptors = function(deviceHandle, characteristicHandle, win, fail) {
			exec(win, fail, 'BLE', 'descriptors', [deviceHandle, characteristicHandle]);
		};*/
	},
	startReading: function(deviceHandle)
		{
			app.updateText('Enabling notifications');

			// Turn notifications on.
			/*
			app.write(
				'writeDescriptor',
				deviceHandle,
				app.descriptorNotification,
				new Uint8Array([1,0]));
			*/
			
			/*evothings.ble.writeDescriptor(
				deviceHandle,
				12,
				new Uint8Array([1,0]),
				function(data){
					app.updateText('success  writeDescriptor : === '+JSON.stringify(data));
				},
				function(data){
					app.updateText('error writeDescriptor : === '+JSON.stringify(data));
				});*/

			// Start reading notifications.
			evothings.ble.enableNotification(
				deviceHandle,
				8,
				function(data)
				{
					app.updateText('notification  : === '+ evothings.ble.fromUtf8(data));
					navigator.notification.beep(2);
				},
				function(errorCode)
				{
					app.updateText('Error notification  : === '+JSON.stringify(data));
				});
		},
	
	formatFlags:function(name, flags, translation)
	{
		var str = name+':';
		for (var key in translation) {
			if((flags & key) != 0)
				str += ' '+translation[key];
		}
		return str;
	}


}