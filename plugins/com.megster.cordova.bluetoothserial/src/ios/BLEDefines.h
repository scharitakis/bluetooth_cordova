
/*
 
 Copyright (c) 2013 RedBearLab
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 */

// RBL Service
#define RBL_SERVICE_UUID                        "A495FF10-C5B1-4B44-B512-1370F02D74DE"
#define RBL_CHAR_TX_UUID                        "A495FF11-C5B1-4B44-B512-1370F02D74DE"
#define RBL_CHAR_RX_UUID                      	"A495FF11-C5B1-4B44-B512-1370F02D74DE"

// Adafruit BLE
// http://learn.adafruit.com/getting-started-with-the-nrf8001-bluefruit-le-breakout/adding-app-support
// Adafruit | Nordic's TX and RX are the opposite of RBL. This code uses RBL perspective for naming.
//#define ADAFRUIT_SERVICE_UUID                         "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
//#define ADAFRUIT_CHAR_TX_UUID                         "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
//#define ADAFRUIT_CHAR_RX_UUID                         "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"

/*
#define ADAFRUIT_SERVICE_UUID                         "E8402D66-BADC-A022-F13C-FB5E9D6A76B8"
#define ADAFRUIT_CHAR_TX_UUID                         "E8402D66-BADC-A022-F13C-FB5E9D6A76B8"
#define ADAFRUIT_CHAR_RX_UUID                         "E8402D66-BADC-A022-F13C-FB5E9D6A76B8"
*/

#define ADAFRUIT_SERVICE_UUID                         "A495FF10-C5B1-4B44-B512-1370F02D74DE"
#define ADAFRUIT_CHAR_TX_UUID                         "A495FF11-C5B1-4B44-B512-1370F02D74DE"
#define ADAFRUIT_CHAR_RX_UUID                         "A495FF11-C5B1-4B44-B512-1370F02D74DE"

#define RBL_BLE_FRAMEWORK_VER                    0x0200
